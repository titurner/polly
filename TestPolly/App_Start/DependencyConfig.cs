﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using TestPolly.Controllers;

namespace TestPolly.App_Start
{
    public class DependencyConfig
    {
        public static void Configure(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();
            
            builder.Register(
                c =>
                    ConfigurationManager.GetSection("cyberSourceCircuitBreakerConfigurationSection") as
                        CyberSourceCircuitBreakerConfigurationSection)
                .As<ICyberSourceCircuitBreakerSettings>();

            builder.RegisterType<CyberSourceCircuitBreaker>().AsSelf().SingleInstance();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // Build the Autofac container
            var container = builder.Build();

            // Wire-up the WebApi DependencyResolver
            httpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}