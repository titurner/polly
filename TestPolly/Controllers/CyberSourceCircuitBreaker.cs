﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.EntitySql;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Polly;
using Polly.CircuitBreaker;

namespace TestPolly.Controllers
{
    public class CyberSourceCircuitBreaker
    {
        private readonly ICyberSourceCircuitBreakerSettings _settings;

        public CyberSourceCircuitBreaker(ICyberSourceCircuitBreakerSettings settings)
        {
            _settings = settings;
            _breaker = new Lazy<CircuitBreakerPolicy>(() =>
            {
                Action<Exception, TimeSpan, Context> onBreak =
                    (exception, timespan, context) => { OnBreak(); };
                Action<Context> onReset = context => { OnReset(); };
                Action onHalfOpen = OnHalfOpen;

                CircuitBreakerPolicy breaker = Polly.Policy
                    .Handle<TimeoutException>()
                    .Or<DivideByZeroException>()
                    .AdvancedCircuitBreakerAsync(_settings.FailureThreshold,
                        TimeSpan.FromSeconds(_settings.SamplingDurationSeconds), _settings.MinimumThroughput,
                        TimeSpan.FromSeconds(_settings.BreakDurationSeconds), onBreak, onReset, onHalfOpen);

                return breaker;
            });
        }

        private readonly Lazy<CircuitBreakerPolicy> _breaker;

        public CircuitBreakerPolicy Policy => _breaker.Value;

        private static void OnHalfOpen()
        {
            Debug.WriteLine("Polly Half Open");
        }

        private static void OnReset()
        {
            Debug.WriteLine("Polly Reset");
        }

        private static void OnBreak()
        {
            Debug.WriteLine("Polly OnBreak");
        }


    }
}
