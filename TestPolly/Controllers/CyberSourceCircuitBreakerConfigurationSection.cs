using System.Configuration;

namespace TestPolly.Controllers
{
    public class CyberSourceCircuitBreakerConfigurationSection : ConfigurationSection,
        ICyberSourceCircuitBreakerSettings
    {
        [ConfigurationProperty("failureThreshold", IsRequired = true)]
        public double FailureThreshold
        {
            get { return (double) this["failureThreshold"]; }
            set { this["failureThreshold"] = value; }
        }

        [ConfigurationProperty("samplingDurationSeconds", IsRequired = true)]
        public int SamplingDurationSeconds
        {
            get { return (int) this["samplingDurationSeconds"]; }
            set { this["samplingDurationSeconds"] = value; }
        }

        [ConfigurationProperty("minimumThroughput", IsRequired = true)]
        public int MinimumThroughput
        {
            get { return (int) this["minimumThroughput"]; }
            set { this["minimumThroughput"] = value; }
        }

        [ConfigurationProperty("breakDurationSeconds", IsRequired = true)]
        public int BreakDurationSeconds
        {
            get { return (int) this["breakDurationSeconds"]; }
            set { this["breakDurationSeconds"] = value; }
        }
    }
}