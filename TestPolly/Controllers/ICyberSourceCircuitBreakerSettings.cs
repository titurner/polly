﻿namespace TestPolly.Controllers
{
    public interface ICyberSourceCircuitBreakerSettings
    {
        double FailureThreshold { get; set; }
        int SamplingDurationSeconds { get; set; }
        int MinimumThroughput { get; set; }
        int BreakDurationSeconds { get; set; }

    }
}