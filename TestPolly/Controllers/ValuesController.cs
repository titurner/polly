﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.SessionState;
using Microsoft.Owin.Security.Provider;
using Polly;
using Polly.CircuitBreaker;


namespace TestPolly.Controllers
{
    
    public class ValuesController : ApiController
    {
        private readonly CyberSourceCircuitBreaker _cyberSourceCircuitBreaker;
        // GET api/values
        public async Task<IEnumerable<string>> Get()
        {
            return
                await
                    _cyberSourceCircuitBreaker.Policy.ExecuteAsync(
                        () => Task.Run(() => new string[] {"value1", "value2"}));
        }      

        public ValuesController(CyberSourceCircuitBreaker policy)
        {
            if (policy == null)
            {
                throw new ArgumentNullException(nameof(policy));
            }
            _cyberSourceCircuitBreaker = policy;
        }


        // GET api/values/5
        public async Task<string> Get(int id)
        {            
            return await _cyberSourceCircuitBreaker.Policy.ExecuteAsync(GetString);            
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        private Task<string> GetString()
        {
            if (DateTime.Now.Millisecond > 499) throw new DivideByZeroException("You divided by 0, what did you do");
            return Task.Run(() => "value");
        }
    }
}
